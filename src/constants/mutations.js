export default {
  SET_USER: 'setUser',
  SET_TOKEN: 'setToken',
  FETCH_PLAYERS_REQUEST: 'fetchPlayersRequest',
  FETCH_PLAYERS_SUCCESS: 'fetchPlayersSuccess',
  FETCH_PLAYERS_FAILURE: 'fetchPlayersFailure',
};
