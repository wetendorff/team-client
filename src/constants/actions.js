export default {
  SET_TOKEN: 'setToken',
  SET_USER: 'setUser',
  FETCH_PLAYERS: 'fetchPlayers',
};
