import Api from '@/services/Api';

export default {
  getAllPlayersByUserId(userId, token) {
    return Api().get(`/v1/users/${userId}/players`, {
      headers: {
        Authorization: token,
      },
    });
  },
};
