import Api from '@/services/Api';

export default {
  createClub(payload, token) {
    return Api().post('/v1/clubs', payload, {
      headers: {
        Authorization: token,
      },
    });
  },
};
