import Api from '@/services/Api';

export default {
  register(credentials) {
    return Api().post('/v1/users', credentials);
  },
  login(credentials) {
    return Api().post('/v1/users/login', credentials);
  },
};
