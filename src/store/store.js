import Vue from 'vue';
import Vuex from 'vuex';
import mutation from '@/constants/mutations';
import PlayerService from '@/services/PlayerService';

Vue.use(Vuex);

export default new Vuex.Store({
  strict: true,
  state: {
    token: localStorage.getItem('token'),
    user: JSON.parse(localStorage.getItem('user')),
    isUserLoggedIn: !!localStorage.getItem('token'),
    players: null,
    playersLoading: false,
  },
  mutations: {
    setToken(state, token) {
      state.token = token;
      state.isUserLoggedIn = !!token;
      localStorage.setItem('token', token);
    },
    setUser(state, user) {
      state.user = user;
      localStorage.setItem('user', JSON.stringify(user));
    },
    fetchPlayersSuccess(state, data) {
      state.players = data;
      state.playersLoading = false;
    },
    fetchPlayersFailure(state) {
      state.players = null;
      state.playersLoading = false;
    },
    fetchPlayersRequest(state) {
      state.playersLoading = true;
    },
  },
  actions: {
    setToken({ commit }, token) {
      commit(mutation.SET_TOKEN, token);
    },
    setUser({ commit }, user) {
      commit(mutation.SET_USER, user);
    },
    async fetchPlayers({ commit }) {
      commit(mutation.FETCH_PLAYERS_REQUEST);
      try {
        const { data } = await PlayerService.getAllPlayersByUserId(
          this.state.user.id,
          this.state.token,
        );
        commit(mutation.FETCH_PLAYERS_SUCCESS, data);
      } catch (err) {
        commit(mutation.FETCH_PLAYERS_FAILURE);
      }
    },
  },
});
