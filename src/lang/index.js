const messages = {
  en: {
    common: {
      readMore: 'Read more',
    },
    message: {
      myPlayers: 'My players',
      myClubs: 'My clubs',
      myTeams: 'My teams',
    },
    usp: {
      organise: 'Organise',
      communicate: 'Communicate',
      connect: 'Connect',
    },
  },
  da: {
    common: {
      readMore: 'Læs mere',
    },
    message: {
      myPlayers: 'Mine spillere',
      myClubs: 'Mine klubber',
      myTeams: 'Mine hold',
    },
    usp: {
      organise: 'Organisere',
      communicate: 'Kommunikere',
      connect: 'Forbinde',
    },
  },
};

export default messages;
