// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import { sync } from 'vuex-router-sync';
import store from '@/store/store';
import ElementUI from 'element-ui';
import locale from 'element-ui/lib/locale/lang/da';
import '@/styles/style.scss';
import i18n from '@/i18nSetup';
import App from './App';
import router from './router';

Vue.use(ElementUI, { locale });
Vue.config.productionTip = false;

sync(store, router);

/* eslint-disable no-new */
new Vue({
  el: '#app',
  i18n,
  router,
  store,
  components: { App },
  template: '<App/>',
});
